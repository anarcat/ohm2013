To make this work, you need to first download the missing videos:

ln -sf ../../media/greve2012/22mars-rouge-9CgD442YFRQ.webm
ln -fs ../../media/greve2012/AlerteRouge.webm
ln -sf ../../media/greve2012/villeray-desobeit-fKsmwyb2pyw.webm
cclive -O ../../media/greve2012/22mars-rouge-9CgD442YFRQ.webm https://youtu.be/9CgD442YFRQ
cclive -O ../../media/greve2012/villeray-desobeit-fKsmwyb2pyw.webm https://youtu.be/fKsmwyb2pyw

then compile a dev version of pinpoint:

cd ~/dist/
git clone http://git.gnome.org/browse/pinpoint
cd pinpoint/
git co -b working f69191d
sudo apt-get install libclutter-1.0-dev libclutter-gst-dev
./autogen.sh ; make
sudo make install
/usr/local/binpinpoint -s ohm2013-maple-spring.pin
